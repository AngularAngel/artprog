package org.codebuddies.artprog.domains;

import org.codebuddies.artprog.DynamicFunction;
import org.codebuddies.artprog.RandomBuilder;

public class FloatArithmetic {
    public static void run() {
        RandomBuilder builder = new RandomBuilder(0x76876547654367L*1);
            
        builder.primitives.add(new DynamicFunction<Double>("-4", new Class[] {}, Double.class, (o) -> {return -4.0;}));
        builder.primitives.add(new DynamicFunction<Double>("-3", new Class[] {}, Double.class, (o) -> {return -3.0;}));
        builder.primitives.add(new DynamicFunction<Double>("-2", new Class[] {}, Double.class, (o) -> {return -2.0;}));
        builder.primitives.add(new DynamicFunction<Double>("-1", new Class[] {}, Double.class, (o) -> {return -1.0;}));
        builder.primitives.add(new DynamicFunction<Double>("0", new Class[] {}, Double.class, (o) -> {return 0.0;}));
        builder.primitives.add(new DynamicFunction<Double>("1", new Class[] {}, Double.class, (o) -> {return 1.0;}));
        builder.primitives.add(new DynamicFunction<Double>("2", new Class[] {}, Double.class, (o) -> {return 2.0;}));
        builder.primitives.add(new DynamicFunction<Double>("3", new Class[] {}, Double.class, (o) -> {return 3.0;}));
        builder.primitives.add(new DynamicFunction<Double>("4", new Class[] {}, Double.class, (o) -> {return 4.0;}));

        builder.primitives.add(new DynamicFunction<Double>("-", new Class[] {Double.class}, Double.class, (o) -> {return -(Double)o[0];}));
        builder.primitives.add(new DynamicFunction<Double>("exp", new Class[] {Double.class}, Double.class, (o) -> {return Math.exp((Double)o[0]);}));
        builder.primitives.add(new DynamicFunction<Double>("sin", new Class[] {Double.class}, Double.class, (o) -> {return Math.sin((Double)o[0]);}));
        builder.primitives.add(new DynamicFunction<Double>("cos", new Class[] {Double.class}, Double.class, (o) -> {return Math.cos((Double)o[0]);}));
        builder.primitives.add(new DynamicFunction<Double>("tan", new Class[] {Double.class}, Double.class, (o) -> {return Math.tan((Double)o[0]);}));
        builder.primitives.add(new DynamicFunction<Double>("+", new Class[] {Double.class, Double.class}, Double.class, (o) -> {return (Double)o[0] + (Double)o[1];}));
        builder.primitives.add(new DynamicFunction<Double>("-", new Class[] {Double.class, Double.class}, Double.class, (o) -> {return (Double)o[0] - (Double)o[1];}));
        builder.primitives.add(new DynamicFunction<Double>("*", new Class[] {Double.class, Double.class}, Double.class, (o) -> {return (Double)o[0] * (Double)o[1];}));
        builder.primitives.add(new DynamicFunction<Double>("/", new Class[] {Double.class, Double.class}, Double.class, (o) -> {return (Double)o[0] / (Double)o[1];}));
        
        builder.evolve(Double.class, 8);
    }
}