package org.codebuddies.artprog.domains;

import org.codebuddies.artprog.DynamicFunction;
import org.codebuddies.artprog.RandomBuilder;

public class IntegerArithmetic {
    public static void run() {
        RandomBuilder builder = new RandomBuilder(0x76876547654367L*1);
            
        builder.primitives.add(new DynamicFunction<Integer>("-4", new Class[] {}, Integer.class, (o) -> {return -4;}));
        builder.primitives.add(new DynamicFunction<Integer>("-3", new Class[] {}, Integer.class, (o) -> {return -3;}));
        builder.primitives.add(new DynamicFunction<Integer>("-2", new Class[] {}, Integer.class, (o) -> {return -2;}));
        builder.primitives.add(new DynamicFunction<Integer>("-1", new Class[] {}, Integer.class, (o) -> {return -1;}));
        builder.primitives.add(new DynamicFunction<Integer>("0", new Class[] {}, Integer.class, (o) -> {return 0;}));
        builder.primitives.add(new DynamicFunction<Integer>("1", new Class[] {}, Integer.class, (o) -> {return 1;}));
        builder.primitives.add(new DynamicFunction<Integer>("2", new Class[] {}, Integer.class, (o) -> {return 2;}));
        builder.primitives.add(new DynamicFunction<Integer>("3", new Class[] {}, Integer.class, (o) -> {return 3;}));
        builder.primitives.add(new DynamicFunction<Integer>("4", new Class[] {}, Integer.class, (o) -> {return 4;}));

        builder.primitives.add(new DynamicFunction<Integer>("-", new Class[] {Integer.class}, Integer.class, (o) -> {return -(Integer)o[0];}));
        builder.primitives.add(new DynamicFunction<Integer>("+", new Class[] {Integer.class, Integer.class}, Integer.class, (o) -> {return (Integer)o[0] + (Integer)o[1];}));
        builder.primitives.add(new DynamicFunction<Integer>("-", new Class[] {Integer.class, Integer.class}, Integer.class, (o) -> {return (Integer)o[0] - (Integer)o[1];}));
        builder.primitives.add(new DynamicFunction<Integer>("*", new Class[] {Integer.class, Integer.class}, Integer.class, (o) -> {return (Integer)o[0] * (Integer)o[1];}));
        builder.primitives.add(new DynamicFunction<Integer>("&", new Class[] {Integer.class, Integer.class}, Integer.class, (o) -> {return (Integer)o[0] & (Integer)o[1];}));
        builder.primitives.add(new DynamicFunction<Integer>("|", new Class[] {Integer.class, Integer.class}, Integer.class, (o) -> {return (Integer)o[0] | (Integer)o[1];}));
        builder.primitives.add(new DynamicFunction<Integer>("^", new Class[] {Integer.class, Integer.class}, Integer.class, (o) -> {return (Integer)o[0] ^ (Integer)o[1];}));
        builder.primitives.add(new DynamicFunction<Integer>("<<", new Class[] {Integer.class, Integer.class}, Integer.class, (o) -> {return (Integer)o[0] << (Integer)o[1];}));
        builder.primitives.add(new DynamicFunction<Integer>(">>", new Class[] {Integer.class, Integer.class}, Integer.class, (o) -> {return (Integer)o[0] >> (Integer)o[1];}));
        
        builder.evolve(Integer.class, 8);
    }
}