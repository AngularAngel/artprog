package org.codebuddies.artprog;

import java.util.ArrayList;

public class TreeNode<THING> {
	public final ArrayList<TreeNode<THING>> children = new ArrayList<>();
    public final THING thing;

    public TreeNode(THING thing) {
        this.thing = thing;
    }

    public int depth() {
		if(children.size() == 0) {
			return 1;
		} else {
			int max = 0;
			for(var child : children) {
				max = Math.max(max, child.depth());
			}
			return max + 1;
		}
	}
}