package org.codebuddies.artprog;

import java.util.function.Function;
import java.util.function.Supplier;

public class DynamicFunction<RETURN> {
    public final String name;
    public final Class[] arguments;
    public final Class<RETURN> returnValue;
    private final Function<Object[], RETURN> function;

    public DynamicFunction(String name, Class[] arguments, Class<RETURN> returnValue, Function<Object[], RETURN> function) {
        this.name = name;
        this.arguments = arguments;
        this.returnValue = returnValue;
        this.function = function;
    }

    public RETURN call(Object[] args) {
        return function.apply(args);
    }

    public Supplier<RETURN> compose(Supplier<Object>... inputs) {
        return () -> {
            Object[] args = new Object[inputs.length];
            for (int i = 0; i < args.length; i++)
                args[i] = inputs[i].get();
            return function.apply(args);
        };
    }
}