package org.codebuddies.artprog;

import org.codebuddies.artprog.domains.FloatArithmetic;
import org.codebuddies.artprog.domains.IntegerArithmetic;

public class Main {
    public static void main(String[] args) throws Exception {
        FloatArithmetic.run();
    }
}