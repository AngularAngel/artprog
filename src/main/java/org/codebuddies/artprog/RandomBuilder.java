package org.codebuddies.artprog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.function.Supplier;

public class RandomBuilder {
	public final Random rand;
    public final ArrayList<DynamicFunction> primitives = new ArrayList<>();

    public RandomBuilder(long seed) {
		rand = new Random(seed);
    }

    public <T> DynamicFunction<T> getPrimitive(Class<T> expectedReturn, boolean zeroArguments) {
        DynamicFunction[] validPrimitives = primitives.stream().filter((fun) -> {
			return fun.returnValue == expectedReturn && (!zeroArguments || fun.arguments.length == 0);
		}).toArray((len) -> new DynamicFunction[len]);
        return validPrimitives[rand.nextInt(validPrimitives.length)];
    }

	

    public <T> TreeNode<DynamicFunction<T>> build(Class<T> expectedReturn, final int maxRecursionDepth) {
        TreeNode<DynamicFunction<T>> root = new TreeNode<DynamicFunction<T>>(getPrimitive(expectedReturn, maxRecursionDepth == 0));
	        
		for(int i = 0; i < root.thing.arguments.length; i++) {
			root.children.add(build(root.thing.arguments[i], maxRecursionDepth - 1));
		}
        return root;
	}

    public <T> Supplier<T> compose(TreeNode<DynamicFunction<T>> root) {
        Supplier[] inputs = new Supplier[root.thing.arguments.length];
        for(int i = 0; i < root.thing.arguments.length; i++) {
			inputs[i] = compose(root.children.get(i));
		}
        return root.thing.compose(inputs);
    }

    public <T> String toString(TreeNode<DynamicFunction<T>> root) {
		if(root.children.size() == 0) {
			return root.thing.name;
		} else if(root.children.size() == 2) {
			String res = "(";
			res += toString(root.children.get(0));
			res += " ";
			res += root.thing.name;
			res += " ";
			res += toString(root.children.get(1));
			res += ")";
			return res;
		} else {
			String res = root.thing.name;
			res += "(";
			for(var child : root.children) {
				res += toString(child);
				res += ", ";
			}
			res = res.substring(0, res.length()-2);
			res += ")";
			return res;
		}
	}

    public <T> int len(TreeNode<DynamicFunction<T>> root) {
		if(root.children.size() == 0) {
			return 1;
		} else {
			int res = 1;
			for(var child : root.children) {
				res += len(child);
			}
			return res;
		}
	}

    private static class PopulationMember<T> {
        public final TreeNode<DynamicFunction<T>> root;
		public double lastScore = 0;
		public int depth;
        public PopulationMember(TreeNode<DynamicFunction<T>> root) {
            this.root = root;
			this.depth = root.depth();
        }
    }

    public <T> TreeNode<DynamicFunction<T>> mutate(TreeNode<DynamicFunction<T>> template, Class<T> expectedReturn, final int maxRecursionDepth) {
		int depth = template.depth();
		if(depth < maxRecursionDepth && rand.nextInt(16) == 0) {
			TreeNode<DynamicFunction<T>> root = new TreeNode<DynamicFunction<T>>(getPrimitive(expectedReturn, false));
			if(root.thing.arguments.length != 0) {
				int positionToInsertOldThing = rand.nextInt(root.thing.arguments.length);
				for(int i = 0; i < root.thing.arguments.length; i++) {
					if(i == positionToInsertOldThing) {
						root.children.add(mutate(template, root.thing.arguments[i], maxRecursionDepth - 1));
					} else {
						root.children.add(build(root.thing.arguments[i], maxRecursionDepth - 1));
					}
				}
			}
			return root;
		} else if(rand.nextInt(8) == 0) {
            return build(expectedReturn, maxRecursionDepth);
		} else {
			TreeNode<DynamicFunction<T>> root = new TreeNode<DynamicFunction<T>>(template.thing);
				
			for(int i = 0; i < root.thing.arguments.length; i++) {
				root.children.add(mutate(template.children.get(i), root.thing.arguments[i], maxRecursionDepth - 1));
			}
			
			return root;
		}
    }

    public <T> void evolve(Class<T> expectedReturn, final int maxRecursionDepth) {
    	PopulationMember<T>[] population = new PopulationMember[10000];
		int selectedAmount = population.length/10;

        for (int i = 0; i < population.length; i++) {
            population[i] = new PopulationMember(build(expectedReturn, maxRecursionDepth));
		}
        
		for(int gen = 0; gen < 10000; gen++) {
			for(var member : population) {
				member.lastScore = Math.abs((156481 - (double) compose(member.root).get()))*1 + len(member.root) * 0.1;
			}

			Arrays.sort(population, (member1, member2) -> {
                return -Double.compare(member2.lastScore, member1.lastScore);
			});

			System.out.println("Generation " + gen + ": " + toString(population[0].root));
			System.out.println("Error: " + population[0].lastScore);
			System.out.println("Result: " + compose(population[0].root).get());
            System.out.println("–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––");
            
			for(int i = selectedAmount; i < population.length; i++) {
				population[i] = new PopulationMember(mutate(population[i%selectedAmount].root, expectedReturn, maxRecursionDepth));
			}
		}
    }
}